import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';
import {LoadingService} from 'src/app/services/loading.service';
import { MenuController, ModalController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor( private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private menu: MenuController,
    private alertService: AlertService ,
    public loading: LoadingService) { }

  ngOnInit() {
  }

  async login(form: NgForm) {
    this.loading.present();
    const formData = new FormData();
    formData.append('usuario', form.value.usuario);
    formData.append('password', form.value.password);
    this.authService.login(formData).subscribe(
      (data: any) => {
          if (data.status) {

            this.navCtrl.navigateRoot('/locales' );

          } else {
            this.alertService.presentToast('Error :' + data.message);
          }
          this.loading.dismiss();
      },
      error => {
        if (!error.error.status) {
          this.alertService.presentToast(error.message);
        } else {
          this.alertService.presentToast(error.message);
        }
        this.loading.dismiss();
      },
    );

  }

}
