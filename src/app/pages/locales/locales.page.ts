import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AlertService } from 'src/app/services/alert.service';
import { StorageService } from 'src/app/services/storage.service';
import { ApiService } from '../../services/api.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-locales',
  templateUrl: './locales.page.html',
  styleUrls: ['./locales.page.scss'],
})
export class LocalesPage implements OnInit {

  constructor(public api: ApiService,   private alertService: AlertService ,
    private loading: LoadingService,  private navCtrl: NavController,public storage: StorageService) { }
  locales: any;

  async ngOnInit() {
    
    (await this.api.locales()).subscribe(
      (data: any) => {
        if (data.status) {
          this.locales = data.locales;
        } else {
          this.alertService.presentToast('Error :' + data.message);
        }
        this.loading.dismiss();
    },  error => {
      if (!error.error.status) {
        this.alertService.presentToast(error.message);
      } else {
        this.alertService.presentToast(error.message);
      }
      this.loading.dismiss();
    },
    );
  }


  seleccionar(data:any){
    this.storage.removeItem('local');
    this.storage.setObject('local', data).then((response) => {
      console.log(JSON.stringify(data));
      this.navCtrl.navigateRoot('/dashboard');
    });

  }

}
