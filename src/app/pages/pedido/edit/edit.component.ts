import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { ModalistComponent } from 'src/app/component/modalist/modalist.component';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MensajealertService } from 'src/app/services/mensajealert.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit {

  id:any;
  comanda: any;
  detalles: any[];

  constructor(public activatedRoute: ActivatedRoute, public loading:LoadingService, public api: ApiService, 
              public mensajeAlert: MensajealertService, public modal:MensajealertService, public Nav: NavController ,  public modalController: ModalController,) { }

  async ngOnInit() {
   this.loading.present();
   this.comanda = this.api.pedidoEdit;
    this.id = this.activatedRoute.snapshot.paramMap.get('id');

    (await this.api.detalleComanda(this.id)).subscribe(
      (data:any) => {
        if (data.status) {
          this.detalles = data.detalle;
          this.calculateTotals();
          this.loading.dismiss();
        } else {
          this.mensajeAlert.mensaje("ocurrio un error","",data.message);
          this.loading.dismiss();
        }
      },   error => {
        if (!error.error.status) {
          this.mensajeAlert.mensaje("ocurrio un error ","",error.message);
          this.loading.dismiss();
        } else {
          this.mensajeAlert.mensaje("error general","",error.message);
          this.loading.dismiss();
        }
        
      }, 
    );
  }


  async listaGrupos() {

    const modal = await this.modalController.create({
      component: ModalistComponent,backdropDismiss : false, showBackdrop:false,
     
      componentProps: {
        'title': 'Seleccione el tipo',
        'tipo': 'grupo',
        'extra': '',
      },
    });
   
    modal.onDidDismiss().then(async (data) => {
      if (data.data !== null) {
        const grupo = data.data;
      await   this.listaProductos(grupo.id_grupo)
      }
    });

    return await modal.present();
}




async listaProductos(idGrupo:any) {
  const modalProducto = await this.modalController.create({
    component: ModalistComponent,backdropDismiss : false, showBackdrop:false,
  
    componentProps: {
      'title': 'Seleccione el item',
      'tipo': 'producto',
      'id': idGrupo,
      'extra':'',
      'closevent': this.listaGrupos(),
    },
  });
 
  modalProducto.onDidDismiss().then(async (data) => {
    if (data.data !== null || data.data.length > 0) {
      for (var item of data.data) {
        const  findItem =  this.detalles.find(x => x.idProducto === item.idProducto);
        if (findItem !== undefined && findItem !== null ) {
            findItem.cantidad++;
            findItem.total = findItem.cantidad * findItem.precio;
        } else {
          this.detalles.push(item);
        }
        this.calculateTotals();
     
      }
    }
  });

  return await modalProducto.present();
}


  calculate(event: any , item: any ) {
    item.total = event.target.value * item.precio;
    this.calculateTotals();
   }

  calculateTotals() {
    this.comanda.total = 0;
    this.comanda.items = 0;
    this.detalles.forEach(item => {
      
      this.comanda.items = this.comanda.items +1;
       item.total = (item.cantidad * item.precio) ;
       this.comanda.total = this.comanda.total *1 + item.total *1;
    });
   
    this.comanda.total = ((this.comanda.total *12) /100) *1  + this.comanda.total *1
  }

  remove(item: any) {
    const index: number = this.detalles.indexOf(item);
    if (index !== -1) {
      this.detalles.splice(index, 1);
      this.calculateTotals();
    }
  }


  async procesar(){
    this.api.pedidoEdit = this.comanda;
    this.loading.present();
    const pedido = new FormData();
    const mensaje = new FormData();
    pedido.append('detalle', JSON.stringify(this.detalles));
    mensaje.append('asunto', 'Modificacion del pedido#'+ this.id);
    mensaje.append('mensaje', 'Se ha modificado el pedido #'+this.id);
    mensaje.append('pedido', this.id);
    mensaje.append('status','M');

    (await this.api.editComanda(this.id,pedido)).subscribe(
      async (data:any) => {
        if (data.status) {
         
         (await this.api.sendmail(mensaje)).subscribe( (mensajeRespuesta:any) => {
              if(mensajeRespuesta.status) {
                this.loading.dismiss();
                this.mensajeAlert.mensaje("estado comanda","",data.message);
                this.Nav.back();
              } else {
                this.loading.dismiss();
                this.mensajeAlert.mensaje("estado comanda","",data.message );
                this.mensajeAlert.mensaje("estado comanda","",mensajeRespuesta.message );
                this.Nav.back();
              
              }
         });
          
        } else {
          this.loading.dismiss();
          this.mensajeAlert.mensaje("estado comanda","",data.message );
         
        }
      },   error => {
        if (!error.error.status) {
          
          this.mensajeAlert.mensaje("Error","",error.message);
        } else {
          this.mensajeAlert.mensaje("Error","",error.message);
        }
        this.loading.dismiss();
      }, 
    );
  }





}
