import { Component, OnInit } from '@angular/core';
import { count } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MensajealertService } from 'src/app/services/mensajealert.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {

  constructor(public api:ApiService, public mensajeAlert: MensajealertService , public loading:LoadingService , public storage: StorageService) { }
  cajero: any;
  pedidos: any[];
  updating: boolean;
  nothing:boolean;
  async ngOnInit() {
    this.cajero = await this.storage.getObject('cajero').then((response:any)=>{
    return response;
    });
    this.updating = true;
      await this.delay(2000);
      await this.comandas(this.cajero);
  }



async comandas(id:number) {

  (await this.api.comandas(id)).subscribe(
    (data:any) => {
      if (data.status) {
        if(data.comanda !== undefined) {
          data.comanda.forEach(pedido => {
            pedido.total = ((pedido.total *12) /100) *1  + pedido.total *1
          });
        }
         if(data.comanda == undefined) {
             this.nothing = true;
           }
         this.updating = false;
         console.log(this.updating +":" + this.pedidos);
         this.pedidos = data.comanda;
      } else {
        this.mensajeAlert.mensaje("mensaje de estado","",data.message);
        this.nothing = true;
        this.updating = false;
      }
    },error => {
      this.nothing = true;
        this.updating = false;
      if (!error.error.status) {
        this.mensajeAlert.mensaje("Error","",error.message);
      } else {
        this.mensajeAlert.mensaje("Error","",error.message);
      }
    }, 
  );
}

  delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}

async actualizar(id:number) {
  this.updating = true;
  this.pedidos = null;
  this.nothing = false;
  await this.delay(2000);
    (await this.api.comandas(id)).subscribe(
      (data:any) => {
        if (data.status) {
          
          this.pedidos = data.comanda;
          if(data.comanda !== undefined) {
            data.comanda.forEach(pedido => {
              pedido.total = ((pedido.total *12) /100) *1  + pedido.total *1
            });
          }
          if(data.comanda == undefined) {
            this.nothing = true;
          }
         this.updating  =false;
        } else {
          this.updating  =false;
          this.nothing = true;
        this.mensajeAlert.mensaje('Error','','Ocurrio un error al actualizar.')
        }
      },   error => {
        if (!error.error.status) {
          this.updating  =false;
          this.nothing = true;
          this.mensajeAlert.mensaje('Error','',error.message)   
        } else {
          this.updating  =false;
          this.nothing = true;
          this.loading.dismiss();
          this.mensajeAlert.mensaje("ocurrio un error","","error general en servicio");
        }
        
      }, 
    );
}


async remove(id:number) {

  this.mensajeAlert.mensajeYesNo("Confirmacion","","Estas seguro de eliminar la comanda #"+ id ,async ()=> {
    this.loading.present();
    (await this.api.removeComandas(id)).subscribe(
      async (data:any) => {
        if (data.status) {
          const mensaje = new FormData();
          mensaje.append('asunto', 'Anulacion del pedido#'+ id);
          mensaje.append('mensaje', 'Se ha anulado el pedido #'+ id);
          mensaje.append('pedido', id.toString());
          mensaje.append('status','E');
          (await this.api.sendmail(mensaje)).subscribe( (mensajeRespuesta:any) => {
            if(mensajeRespuesta.status) {
               this.mensajeAlert.mensaje('Pedido','',data.message)
               this.actualizar(this.cajero);
                this.loading.dismiss();
            
            } else {
             
              this.mensajeAlert.mensaje('Pedido','',data.message)
              this.actualizar(this.cajero);
              this.loading.dismiss();
            }
       });

      
        } else {
          this.loading.dismiss();
        this.mensajeAlert.mensaje('Error','','Ocurrio un error al eliminar la comanda.');
        }
      },   error => {
        if (!error.error.status) {
          this.loading.dismiss();
          this.mensajeAlert.mensaje('Error','',error.message)   
        } else {
          this.loading.dismiss();
          this.mensajeAlert.mensaje("ocurrio un error","","error general en servicio");
        }
        
      }, 
    );
  });


  
}



setPedido(data){
  this.api.pedidoEdit = data;
}

}
