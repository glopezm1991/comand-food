import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Comanda } from 'src/app/models/comanda.model';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MensajealertService } from 'src/app/services/mensajealert.service';

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.component.html',
  styleUrls: ['./confirmacion.component.scss'],
})
export class ConfirmacionComponent implements OnInit {

  @Input() data: any;
  @Input() mesa: any;
  detalles: any[];

  constructor(private modalCtrl: ModalController, public mensajeAlert: MensajealertService,
              public api: ApiService, public loading: LoadingService ) { }

  ngOnInit() {

   this.detalles = this.data.detalle;
   this.calculateTotals();
  }



  calculate(event: any , item: any ) {
    item.total = event.target.value * item.precio;
    this.calculateTotals();
   }

 
  calculateTotals() {
    this.data.total = 0;
    this.data.items = 0;
    this.detalles.forEach(item => {
      this.data.items = this.data.items +1;
       item.total = (item.cantidad * item.precio) ;
       this.data.total = this.data.total *1 + item.total *1;
    });
   
    this.data.total = ((this.data.total *12) /100) *1  + this.data.total *1
   
  }

  remove(item: any) {
    const index: number = this.detalles.indexOf(item);
    if (index !== -1) {
      this.detalles.splice(index, 1);
    }
  }



  async closeModal(data:any) {
      await this.modalCtrl.dismiss(data);
  }

   async confirmar(){
    this.loading.present();
    const pedido = new FormData();
    pedido.append('local', this.data.local.toString());
    pedido.append('mesa', this.data.mesa.toString());
    pedido.append('mesero', this.data.mesero.toString());
    pedido.append('detalle', JSON.stringify(this.detalles));
    pedido.append('estado', 'A');
    (await this.api.addComanda(pedido)).subscribe(
      (data:any) => {
        if (data.status) {
          this.loading.dismiss();
          this.data.id = data.numpedido;
          this.mesa.EstadoMesa = "OcupadoE";
          this.data.process = true;
          this.closeModal(this.data);
        } else {
          this.mensajeAlert.mensaje("estado comanda","",data.message);
          this.loading.dismiss();
        }
      },   error => {
        if (!error.error.status) {
          this.mensajeAlert.mensaje("Error","",error.message);
        } else {
          this.mensajeAlert.mensaje("Error","",error.message);
        }
        this.loading.dismiss();
      }, 
    );
  }

}
