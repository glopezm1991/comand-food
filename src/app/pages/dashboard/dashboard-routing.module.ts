import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';
import { MesasComponent } from './mesas/mesas.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage,
    children: [
      {
        path: 'mesas',
        component: MesasComponent
      },
      {
        path: 'menu',
        loadChildren: () => import('../menu/menu.module').then(m => m.MenuPageModule)
      },
      {
        path: 'locales',
        loadChildren: () => import('../locales/locales.module').then(m => m.LocalesPageModule)
      },
      {
        path: 'pedidos',
        loadChildren: () => import('../pedido/pedido-routing.module').then(m => m.PedidoPageRoutingModule)
      },
      {
        path: '',
        redirectTo: '/dashboard/mesas',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
