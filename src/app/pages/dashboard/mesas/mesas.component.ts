import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { ModalController } from '@ionic/angular';
import { ModalistComponent } from 'src/app/component/modalist/modalist.component';
import { Comanda } from 'src/app/models/comanda.model';
import { AlertService } from 'src/app/services/alert.service';
import { ApiService } from 'src/app/services/api.service';
import { LoadingService } from 'src/app/services/loading.service';
import { MensajealertService } from 'src/app/services/mensajealert.service';
import { StorageService } from 'src/app/services/storage.service';
import { ConfirmacionComponent } from '../confirmacion/confirmacion.component';

@Component({
  selector: 'app-mesas',
  templateUrl: './mesas.component.html',
  styleUrls: ['./mesas.component.scss'],
})
export class MesasComponent implements OnInit {
  selocal: any;
  mesas: any[]  = [];
  cajero : any;
  comanda: Comanda;
  mst:number;
  date = new Date();
  StateProccessId: any;
  constructor(public storage: StorageService , 
              public modalController: ModalController,
              public AlertService: AlertService, 
              public loading: LoadingService,
              public mensajeAlert: MensajealertService,
              public api: ApiService, private datePipe: DatePipe) {

  }

  dismissLogin() {
    this.modalController.dismiss();
  }
  
  async ngOnInit() {
    this.loading.present();
    this.selocal = await this.storage.getObject('local').then((response:any)=>{
      return response;
    });
   this.cajero = await this.storage.getObject('cajero').then((response:any)=>{
    return response;
  });
    
   await  this.generarmesas(this.selocal.id);
   this.comanda = {process:false, id:0,detalle:[] ,fecha: this.datePipe.transform(this.date,"yyyy-MM-dd"),local:this.selocal.id,mesa:0 , items:0, total:0, mesero: this.cajero };
    
  }


  initComanda() {
    this.comanda = { process:false, id:0,detalle:[] ,fecha: this.datePipe.transform(this.date,"yyyy-MM-dd"),local:this.selocal.id,mesa:0 , items:0, total:0, mesero: this.cajero };
  }

  async verificarMesas(){
    this.loading.present();
    setInterval(async data => {
      this.selocal = await this.storage.getObject('local').then((response:any)=>{
        return response;
      });
     await  this.generarmesas(this.selocal.id);
    
      }, 100000);
     
  }

  async generarmesas(id: any) {
   
    (await this.api.mesas(id)).subscribe(
      (data: any) => {
        if (data.status) {
          this.mesas = data.mesas;
          this.loading.dismiss();
        } else {
          this.AlertService.presentToast('Error :' + data.message);
          this.loading.dismiss();
        }
      
    },  error => {
      if (!error.error.status) {
        this.AlertService.presentToast(error.message);
        this.loading.dismiss();
      } else {
        this.AlertService.presentToast(error.message);
        this.loading.dismiss();
      }
     
    },
    );
  }

  async liberar(mesa: any) {
    this.loading.present();
    const formData = new FormData();
    formData.append('mesero', this.cajero);
    formData.append('mesa', mesa.id);
    (await this.api.liberar(formData)).subscribe(
      (data:any) => {
        if (data.status) {
          mesa.icono = 'lock-open';
          mesa.color = 'secondary';
          mesa.EstadoMesa = 'Liberado';
          mesa.total = 0.00;
          mesa.items = 0;
          mesa.ocupantes = 0;
          this.mensajeAlert.mensaje("mensaje de estado","",data.message);
          this.loading.dismiss();
        } else {
          this.mensajeAlert.mensaje("mensaje de estado","",data.message);
          this.loading.dismiss();
        }
      },   error => {
        if (!error.error.status) {
          this.mensajeAlert.mensaje("Error","",error.message);
        } else {
          this.mensajeAlert.mensaje("Error","",error.message);
        }
        this.loading.dismiss();
      }, 
    );
  }

  async reservar(mesa:any) {
    if(mesa.EstadoMesa === 'Liberado') {
      this.mensajeAlert.mensajeActionCustom("ingrese ","Ingrese Ocupantes","Reservar", 
       async (data) => {
        this.loading.present();
        const formData = new FormData();
        const ocupantes :number = data.txt1;
        if ( ocupantes <= 0 ) {
          this.mensajeAlert.mensaje("error","",'Numero de ocupantes debe ser mayor a 0');
          this.loading.dismiss();
          return;
        }

        formData.append('ocupantes', ocupantes.toString() );
        formData.append('mesero', this.cajero);
        formData.append('mesa', mesa.id);
        (await this.api.reservar(formData)).subscribe(
          (data:any) => {
            if (data.status) {
              mesa.ocupantes =   ocupantes
              mesa.icono = 'lock-closed';
              mesa.color = 'warning';
              mesa.EstadoMesa = 'Ocupado';
              this.mensajeAlert.mensaje("mensaje de estado","",data.message);
              this.loading.dismiss();
            } else {
              this.mensajeAlert.mensaje("mensaje de estado","",data.message);
              this.loading.dismiss();
            }
          },   error => {
            if (!error.error.status) {
              this.mensajeAlert.mensaje("Error","",error.message);
            } else {
              this.mensajeAlert.mensaje("Error","",error.message);
            }
            this.loading.dismiss();
          }, 
        );
        
        });
      } else if(mesa.EstadoMesa=='OcupadoP') {
             this.mensajeAlert.mensajeYesNo("Pedido","El pedido se descartara automaticamente",'Desea continuar?',async ()=>{
                this.initComanda();
                mesa.EstadoMesa=='OcupadoD';
                await this.liberar(mesa);
             });
      } 
      else if(mesa.EstadoMesa=='OcupadoE') {
        this.mensajeAlert.mensaje("Pedido","Proceso no es posible",'No es posible realizar el proceso porque el pedido ya fue enviado');
       } 
        
    else if (mesa.EstadoMesa=='Ocupado' || mesa.EstadoMesa=='OcupadoD'  ) {

     await this.liberar(mesa);
     
    }
  }

  async listaGrupos(mesa: any) {
    this.dismissLogin();
    this.mst = mesa.id;
    const modal = await this.modalController.create({
      component: ModalistComponent,backdropDismiss : false, showBackdrop:false,
     
      componentProps: {
        'title': 'Seleccione el tipo',
        'tipo': 'grupo',
        'extra': mesa,
      },
    });
   
    modal.onDidDismiss().then(async (data) => {
      if (data.data !== null) {
        const grupo = data.data;
      await   this.listaProductos(grupo.id_grupo,mesa)
      }
    });

    return await modal.present();
}




async listaProductos(idGrupo:any, mesa: any) {
  const modalProducto = await this.modalController.create({
    component: ModalistComponent,backdropDismiss : false, showBackdrop:false,
  
    componentProps: {
      'title': 'Seleccione el item',
      'tipo': 'producto',
      'id': idGrupo,
      'extra':mesa,
      'closevent': this.listaGrupos(mesa),
    },
  });
 
  modalProducto.onDidDismiss().then(async (data) => {
    if (data.data !== null || data.data.length > 0) {
      for (var item of data.data) {
        item.fechaModifica = this.datePipe.transform(this.date,"yyyy-MM-dd");
        this.comanda.detalle.push(item);
        this.comanda.mesa = mesa.id;
        mesa.EstadoMesa = "OcupadoP"
      }
    }
  });
  return await modalProducto.present();
}


async ModalConfirmar(data: any, mesa: any) {
  const modalConfirmar = await this.modalController.create({
    component: ConfirmacionComponent,backdropDismiss : false, showBackdrop:false,
  
    componentProps: {  
      'data': data,
      'mesa':mesa,
    },
  });
 
  modalConfirmar.onDidDismiss().then(async (data) => {
    if (data.data) {
      if (data.data !== null || data.data.process) {
         mesa.EstadoMesa = "OcupadoE";
         mesa.total = data.data.total;
         mesa.items = data.data.items;
         this.initComanda()
        this.mensajeAlert.mensaje("Confirmacion exitosa","","Comanda ha sido generada correctamente");
      }
      
    }
  });
  return await modalConfirmar.present();
}


async add(mesa:any) {
  if ( this.mst == mesa.id || this.mst == 0) {
      await this.listaGrupos(mesa);
  } else {
      this.initComanda();
      await this.listaGrupos(mesa);
  }
}





async enviar(mesa:any) {
 await this.ModalConfirmar(this.comanda,mesa);
}


viewStateMesa() {
  this. StateProccessId = setInterval(async () =>{
    console.log('verificado estado...');
    (await this.api.estadoMesas()).subscribe(
      (data:any) => {
        if (data.status) {
          const  findMesa =  this.mesas.find(x => x.id === data.id);
          if (findMesa !== undefined && findMesa !== null ) {
               if (data.total <= 0  ) {
                 findMesa.EstadoMesa = 'Ocupado';
               }
        } 
      }
      },   error => {
        if (!error.error.status) {
          console.log('ocurrio un error  metodo statesMesa');
        } else {
          console.log('ocurrio un error  metodo statesMesa');
        }
        
      }, 
    );
  
  }, 10000);
  
}

}
