import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPageRoutingModule } from './dashboard-routing.module';

import { DashboardPage } from './dashboard.page';
import { ModalistComponent } from 'src/app/component/modalist/modalist.component';
import { MesasComponent } from './mesas/mesas.component';
import { ItemProductoComponent } from 'src/app/component/item-producto/item-producto.component';
import { ConfirmacionComponent } from './confirmacion/confirmacion.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashboardPageRoutingModule
  ],
  declarations: [DashboardPage,MesasComponent,ConfirmacionComponent],
  entryComponents: [
    ModalistComponent,
    ItemProductoComponent
]
})
export class DashboardPageModule {}
