import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';



@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit {
  @Input() data: any;
  image:string;
  constructor(private modalCtrl:ModalController) { }

  ngOnInit() {


    this.image = "http://api.grupojolie.com/images/"+this.data.id +".jpg";
  }



  async closeModal() {
      await this.modalCtrl.dismiss(null);
  }

  onError() {
    this.image = "http://api.grupojolie.com/images/logo.png";
  }
}
