import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ModalistComponent } from 'src/app/component/modalist/modalist.component';
import { ApiService } from 'src/app/services/api.service';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

 local: any;
 numesas: number
 
  constructor(public storage: StorageService , public modalController: ModalController,  public api: ApiService , 
    public navCtrl : NavController , public auth: AuthService) {

  }
  async ngOnInit(): Promise<void> {
    
    this.local = await this.storage.getObject('local').then((response:any)=>{
      return response;
    });
  }

  async logout(){
    await this.auth.logout();
    this.navCtrl.navigateRoot('/login');
  }
 
  
}
