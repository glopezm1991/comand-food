import { Detalle } from "./detalle.model";

export interface Comanda {
    id: number;
    fecha: string;
    local: number;
    mesa:number;
    mesero:number;
    process:boolean;
    items:number;
    total:number;
    detalle : Detalle[];
  }