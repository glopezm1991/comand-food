
export interface Detalle {
    id: number;
    linea: number;
    descripcion:string;
    idProducto: number;
    cantidad:string;
    precio:number;
    total:number;
    observacion:number;
    fechaModifica:string;
    mesero:string;
    estado:string;

}