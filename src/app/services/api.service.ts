import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EnvService } from './env.service';
import { AuthService } from './auth.service';
import { map, timeout } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  pedidos:any[];
  pedidoEdit:any;
  constructor(private http: HttpClient, private env: EnvService ,public auth: StorageService) {

   }

  async locales(){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
       return  this.http.get(this.env.API_URL+ 'company',  { headers }  ).pipe(
      map(data => {
        return data;
      }),
    );
  }

  async mesas(local:any){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
    return  this.http.get(this.env.API_URL+ 'company/'+ local ,  { headers }  ).pipe(
   map(data => {
     return data;
   }),);
  }

  async productos(){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
    return  this.http.get(this.env.API_URL+ 'producto' ,  { headers }  ).pipe(
   map(data => {
     return data;
   }),);
  }


  async grupos(){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
    return  this.http.get(this.env.API_URL+ 'producto/grupo' ,  { headers }  ).pipe(
   map(data => {
     return data;
   }),);
  }

  async productosXgrupo(idGrupo:any){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
    return  this.http.get(this.env.API_URL+ 'producto/grupo/'+idGrupo ,  { headers }  ).pipe(
   map(data => {
     return data;
   }),);
  }

  async reservar(data: any ){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
    return  this.http.post(this.env.API_URL+ 'company/reserva' , data, { headers }  ).pipe(
   map(data => {
     return data;
   }),);
  }

  async liberar(data:any){
    const token = await this.auth.getString('token').then((response:any)=>{ return response;});
    const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
    return  this.http.post(this.env.API_URL+ 'company/liberar' , data ,  { headers }  ).pipe(
   map(data => {
     return data;
   }),);
  }

async addComanda(comanda:any) {
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.post(this.env.API_URL+ 'comanda' , comanda ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
}

async editComanda(id:any ,detalle:any) {
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.post(this.env.API_URL+ 'comanda/update/'+ id , detalle ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
}

async detalleComanda(id:number) {
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.get(this.env.API_URL+ 'comanda/detalle/' + id ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
}

async comandas(id:number) {
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.get(this.env.API_URL+ 'comanda/' + id ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
}

  async removeComandas(id: number) {
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.delete(this.env.API_URL+ 'comanda/' + id ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
 
}



async estadoMesas(){
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.get(this.env.API_URL+ 'company/1/estado' ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
}


async sendmail(mensaje:any) {
  const token = await this.auth.getString('token').then((response:any)=>{ return response;});
  const headers = new HttpHeaders({ authorization : 'Bearer ' + token});
  return  this.http.post(this.env.API_URL+ 'comanda/sendmail' , mensaje ,  { headers }  ).pipe(
 map(data => {
   return data;
 }),);
}

  async addPedido (data:any) {
    const  findItem =  this.pedidos.find(x => x.id === data.id);
    if (findItem !== undefined && findItem !== null ) {
       return false;
    } else {
      this.pedidos.push(data);
     return true;
    }
  }

  



}
