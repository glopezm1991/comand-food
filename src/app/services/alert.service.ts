import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private toastController: ToastController) { }
  async presentToast(messagee: any) {
    const toast = await this.toastController.create({
      message: messagee,
      duration: 2000,
      color: 'secondary'
    });
    toast.present();
  }
}