import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class MensajealertService {

  constructor(public alertController: AlertController) { }

  public async  mensaje(title: string , subtitle: string, mensaje: string ) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }

  public async  mensajeYesNo(title: string , subtitle: string, mensaje: string , actionYes: () => any  ) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: mensaje,
      buttons: [{
        text : 'Si',
        handler: () => {
         actionYes();
        }
      },
      {
        text: 'No',
        role: 'cancel',
      }]
    });

    await alert.present();
  }


  public async  mensajeAction(title: string , mensaje: string, Action: () => any ) {
    const alert = await this.alertController.create({
      header: title,
      message: mensaje,
      buttons: [{
        text : 'OK',
        handler: () => {
         Action();
        }
      }]
    });

    await alert.present();
  }

  public async  mensajeActionCustom(title: string , mensaje: string , textButton: string , Action: (data:any) => any ) {
    const alert = await this.alertController.create({
      header: title,
      message: mensaje,
      inputs: [
        {
          name: 'txt1',
          type: 'number',
          placeholder: '# ocupantes',
          min: 1,
          max: 10
        }],
      buttons: [{
        text : textButton,
        handler: (alertData) => {
         Action(alertData);
        }
      }, {
        text: 'Cancelar',
        role: 'cancel',
      }]
    });

    await alert.present();
  }

}
