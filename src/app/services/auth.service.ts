import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { EnvService } from './env.service';
import { StorageService } from './storage.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authState = new BehaviorSubject(false);
  token: any = '';
  usuario: any;


  constructor(private http: HttpClient, private env: EnvService, private platform: Platform,  public storage: StorageService,) {
    console.log('inicio');
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
   }


 

  login(datafrm: any ) {
    return this.http.post(this.env.API_ENDPOINTAUTH, datafrm ).pipe(
      map(data => {
        this.token = data['token'];
        this.storage.setString('token', data['token']).then((response) => {
          this.authState.next(true);
          
        });
        this.storage.setString('idusuario', data['id']).then((response) => {
        
        });
        this.storage.setString('nombres', data['nombres'] +  ' '+ data['apellidos']).then((response) => {
        
        });
        this.storage.setString('cajero', data['cajero']).then((response) => {
        
        });
        return data;
      }),
    );
  }

  ifLoggedIn() {
    this.storage.getString('token').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }

  async logout() {
   await this.storage.removeItem('token').then(() => {
      this.authState.next(false);
    });
    await this.storage.removeItem('local');
    await this.storage.removeItem('idusuario');
    await this.storage.removeItem('nombres');
    await this.storage.removeItem('cajero');
  }

  isAuthenticated() {
    return this.authState.value;
  }

  async getToken() {
   await this.storage.getString('token').then((val) => {
      return val;
    });
  }


 
}

