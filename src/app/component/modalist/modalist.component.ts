import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ViewComponent } from 'src/app/pages/dashboard/view/view.component';
import { ApiService } from 'src/app/services/api.service';
import { MensajealertService } from 'src/app/services/mensajealert.service';
import { Detalle } from '../../models/detalle.model';
@Component({
  selector: 'app-modalist',
  templateUrl: './modalist.component.html',
  styleUrls: ['./modalist.component.scss'],
})
export class ModalistComponent implements OnInit {

  public data: any [];
  public pedidos: any [] = [];

  @Input() title: string;

  @Input() tipo: string;

  @Input() extra: any;
  
  @Input() id: string;

  
  
  @Output() closevent = new EventEmitter<any>();
  @Output() extraEvent = new EventEmitter<any>();

  constructor(private modalCtrl: ModalController,
              private api: ApiService,
              private modal: MensajealertService,
              private Alert: MensajealertService) {      
  }




    async serviceProducto(id:any) {
    (await this.api.productosXgrupo(id)).subscribe(
      (data: any) => {
        if ( data.status ) {
          this.data =  data.data;
        } else {
          this.Alert.mensajeAction('Error', data.message, () => { this.closeModal(); } );
        }
      },
      error => {
        console.log(error);
        this.Alert.mensajeAction('Error', 'Ocurrio un error en el servicio intentalo luego.', () => { this.closeModal(); } );
      },
    );
   }


   async grupos() {
    (await this.api.grupos()).subscribe(
      (data: any) => {
        if ( data.status ) {
          this.data =  data.data;
        } else {
          this.Alert.mensajeAction('Error', data.message, () => { this.closeModal(); } );
        }
      },
      error => {
        console.log(error);
        this.Alert.mensajeAction('Error', 'Ocurrio un error en el servicio intentalo luego.', () => { this.closeModal(); } );
      },
    );
   }


  ngOnInit() {
  
    switch ( this.tipo ) {
      case 'producto' : this.serviceProducto(this.id); break;
      case 'grupo' : this.grupos(); break;
    
      default : '' ; break;
    }

  }


  async closeModal() {
    if (this.tipo == 'producto'){
      await this.modalCtrl.dismiss(this.pedidos);
    } else {
      await this.modalCtrl.dismiss(null);
    }
    

  }

  async select(data: any) {
    if(this.tipo == 'grupo'){
      await this.modalCtrl.dismiss(data);
    }
  
  }

  

  async updateItem(data: any ,producto: any) {
    let det: Detalle = { descripcion: producto.descripcion, cantidad:data.cantidad,  precio:producto.precio , total:  (producto.precio) * data.cantidad, linea:1,id:0,estado:'A',fechaModifica:'',
          observacion:data.observacion,idProducto:producto.id , mesero: 'N/A' }
   if(this.extra) {
    this.extra.total =  (this.extra.total) *1  + (producto.precio) * data.cantidad;
    this.extra.items =  (this.extra.items) *1  + data.cantidad;
   }
    
    this.Alert.mensaje('agregado','', 'Producto :' + producto.descripcion + ', cantidad :'  + data.cantidad );
    this.pedidos.push(det);
  }

}
