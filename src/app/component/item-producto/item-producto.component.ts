import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ViewComponent } from 'src/app/pages/dashboard/view/view.component';

@Component({
  selector: 'item-producto',
  templateUrl: './item-producto.component.html',
  styleUrls: ['./item-producto.component.scss'],
})
export class ItemProductoComponent implements OnInit {

  @Input() data: any;
  @Input() mesa: any;
  @Output() updateItem = new EventEmitter<any>();
  cantidad: number = 1;
  observacion: string = "";
  constructor( private modalDetalle: ModalController,) { }

  ngOnInit() {}


  add() {
   
    this.updateItem.emit({cantidad: this.cantidad, observacion: this.observacion});
    
  }

  async verDetalle(producto: any) {
 
  
    const modal = await this.modalDetalle.create({
      component: ViewComponent,backdropDismiss : false, showBackdrop:false,
     
      componentProps: {
        'data': producto,
      },
    });
   
    return await modal.present();
}

}
