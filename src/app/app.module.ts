import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Storage } from '@ionic/storage';
import { ModalistComponent } from './component/modalist/modalist.component';
import { ItemProductoComponent } from './component/item-producto/item-producto.component';
import {DatePipe} from '@angular/common';

@NgModule({
  declarations: [AppComponent,ModalistComponent,ItemProductoComponent],
  exports: [ ModalistComponent,ItemProductoComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,HttpClientModule,FormsModule,ReactiveFormsModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy, },Storage, DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
